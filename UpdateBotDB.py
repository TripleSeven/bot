import os
import json
import shutil

PATH_ASSETS = os.path.join(os.path.dirname(
	os.path.dirname(os.path.realpath(__file__))), 'assets')
dest = os.path.join(os.path.dirname(
	os.path.realpath(__file__)), *['res', 'configdata'])


def main():
	os.makedirs(dest, exist_ok=True)
	db = {}
	p_cn = os.path.join(
		PATH_ASSETS, *['china', 'ExportAssetBundle', 'configdata', 'china'])
	p_gl = os.path.join(
		PATH_ASSETS, *['global', 'ExportAssetBundle', 'configdata', 'global'])

	for fp in os.listdir(p_gl):
		f_cn = os.path.join(p_cn, fp)
		f_gl = os.path.join(p_gl, fp)

		data = {
			item['ID']: item
			for item in json.loads(open(f_gl, 'rb').read())
		}


		if os.path.isfile(f_cn):
			Merge(data, {
				item['ID']: item
				for item in json.loads(open(f_cn, 'rb').read())
			})

		db[fp[10:-9]] = data
		open(os.path.join(dest, fp[10:-9]+'.json'), 'wb').write(
			json.dumps(data, ensure_ascii=False, indent='\t').encode('utf8'))


def Merge(gl, cn):
	for key, item in cn.items():
		if key in gl:
			typ = type(item)
			if typ == dict:
				Merge(item, gl[key])
		else:
			gl[key] = item
	return gl


def FixSkill():
	fin = open(dest + "\\Skill.json", "rt", encoding="utf-8")
	data = fin.read()
	#general fix
	data = data.replace('\\n', ' ')
	#fix matthew
	data = data.replace('trigger a <color=#DC143C>20%</color> DEF increase', 'increase ATK & DEF by <color=#DC143C>20%</color>')
	#fix blue star
	data = data.replace('for every <color=#DC143C>1</color> block moved before battle. (Up to', 'for every block moved before battle. (Up to')
	data = data.replace('with every <color=#DC143C>1</color> block moved, ATK, DEF, and MDEF', 'with every block moved, ATK, DEF, and MDEF')
	#fix reaper's touch
	data = data.replace('0.2x</color> magic damage to a single enemy and adds a debuff:', '0.1x</color> magic damage to a single enemy and adds a debuff:')
	#fix calamity
	data = data.replace('drains a maximum of <color=#DC143C>2</color> buffs from the enemy', 'steals a maximum of <color=#DC143C>2</color> buffs from the enemy')
	#fix plunder
	data = data.replace('removes up to <color=#DC143C>3</color> buffs from the enemy', 'steals up to <color=#DC143C>3</color> buffs from the enemy')
	#fix alustriel
	data = data.replace('soldier damage is not reduced in melee battles', 'unit damage is not reduced in melee battles')
	#fix emerald crusher
	data = data.replace('after battle, dispels all debuffs on the target', 'after battle, reapplies dispelled debuffs to the target')
	#fix sleep
	data = data.replace('0.25</color>x physical damage to a single enemy', '0.25</color>x magic damage to a single enemy')
	#fix killing blow
	data = data.replace('deal damage directly to the enemy hero', 'ignore guard and deal damage directly to the enemy hero')
	#fix charge
	data = data.replace('block moved, damage increases by <color=#DC143C>5%', 'block moved, ATK increases by <color=#DC143C>5%')
	#fix lightning flash
	data = data.replace('You can take action again, but are inflicted', 'If target is not killed, you can take action again, but are inflicted')
	#fix regroup
	data = data.replace('soldier who has already performed their action and died to perform this action again', 'ally who has no soldiers left to act again')
	#fix entrenchment
	data = data.replace('Immobilized. If unit dies while guarding', 'Immune to shove effects. If unit dies while guarding')
	#fix selena
	data = data.replace('Choose a fallen soldier within <color=#DC143C>3</color>-block to take all attacks directed at allied forces', 'Take all attacks for allies within <color=#DC143C>3</color> blocks who have lost all their soldiers')
	#fix wiler
	data = data.replace('When a hero dies, dispels <color=#DC143C>2</color> debuffs on all allies within <color=#DC143C>5</color> blocks and gains', 'When this hero dies, dispels <color=#DC143C>2</color> debuffs from all allies within <color=#DC143C>5</color> blocks and grants')
	#fix rachel
	data = data.replace('2</color> blocks and heals for <color=#DC143C>250%', '3</color> blocks and heals for <color=#DC143C>250%')
	#fix omega
	data = data.replace('After attacking an enemy unit, teleport back to the pre-attack position and gain', 'After defeating an enemy unit, teleport back to the pre-attack position and gain')
	data = data.replace('damage. If the enemy is Weakened, ignores', 'damage. If the enemy is debuffed, ignores')

	fin.close()
	fin = open(dest + "\\Skill.json", "wt", encoding="utf-8")
	fin.write(data)
	fin.close()


def FixHero():
	fin = open(dest + "\\Hero.json", "rt", encoding="utf-8")
	data = fin.read()
	#remove spaces in names
	data = data.replace('Silver Wolf', 'SilverWolf', 1)
	data = data.replace('Sumire Kanzaki', 'SumireKanzaki', 1)
	data = data.replace('Sakura Shinguji', 'SakuraShinguji', 1)
	data = data.replace('Gerold & Layla', 'Gerold&Layla', 1)

	fin.close()
	fin = open(dest + "\\Hero.json", "wt", encoding="utf-8")
	fin.write(data)
	fin.close()


main()
FixSkill()
FixHero()
