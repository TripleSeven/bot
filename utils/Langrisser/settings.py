global PAGES
PAGES={
    'hero':{
        '🗃':   'main',
        '📰':  'lore',
        '🇮':   'image',
        '🇸':   'skills',
        '🇹':   'troops',
        '🇲':   'mats',
        '0⃣':   'class0',
        '1⃣':   'class1',     
        '2⃣':   'class2',
        '3⃣':   'class3',
        '4⃣':   'class4',
        '5⃣':   'class5',
        '6⃣':   'class6',
        #'7⃣':   'class7,'
    },
    'enchant':{
        '⚔':'Weapon',
        '👘':'Armor',
        '⛑':'Helmet',
        '💍':'Accessories',
        '💯':'Chances'
    }
}
