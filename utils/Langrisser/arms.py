from ._main import DIRS,Embed, DIRS2, gitlink, SkillDescriptionAIO, STAT_TYPES
import math

def Arms2(iname):
    #get gear dir
    gear=DIRS2['Equipment'][iname]
    #create basic embed
    embed= Embed(
        title=gear['Name'], #page name
        #url=gear['url']  #page link
        )
    embed.set_thumbnail(url=gitlink(gear['Icon']))
    fields=[
        {'name':'Type'   ,'value':gear['EquipmentType'][14:] ,'inline':True},
        {'name':'Rarity'   ,'value':DIRS2['Rank'][gear['Rank']]['Desc'] ,'inline':True},
        {'name':'Description'   ,'value':gear['Desc'] ,'inline':True},
    ]
    if 'ArmyIds' in gear and gear['ArmyIds'][0] != -1:
        fields.insert(2, {'name':'Equipable',    'value':    ', '.join([DIRS2['Army'][i]['Name'] for i in gear['ArmyIds']]), 'inline':False})
    if 'SkillIds' in gear:
        fields.append({'name':'Skill', 'value': SkillDescriptionAIO(gear['SkillIds']), 'inline':False})

    fields+=[
        {'name':'Stats'   ,'value':'```%s```'%'\n'.join([
            'Lv %s:\t%s'%(lv, CalcArmsStats(gear, lv))
            for lv in [10,20,30,40,50]
            ]) ,'inline':False},
    ]

    embed.ConvertFields(fields)
    return embed

def CalcArmsStats(arms, lv):
    id='Property%sID'
    ini='Property%sValue'
    up='Property%sLevelUpValue'

    return ', '.join([
        '%s %s'%(
            round(arms[ini%i] + (lv-1)*arms[up%i]/10),
            STAT_TYPES[arms[id%i][25:-3]]
        )
        for i in range(1,4)
        if id%i in arms and arms[id%i][-4:]!='None'
    ])

def Arms(iname):    #wiki version
    #get gear dir
    gear=DIRS['arms'][iname]
    #create basic embed
    embed= Embed(
        title=gear['name'], #page name
        url=gear['url']  #page link
        )
    #embed.set_thumbnail(url='http://cdn.alchemistcodedb.com/images/items/icons/{}.png'.format(gear['icon']))
    embed.set_thumbnail(url=gear['img'])
    fields=[
        {'name':'Type'   ,'value':gear['type'] ,'inline':True},
        {'name':'Rarity'   ,'value':gear['rarity'] ,'inline':True}
        ]
    if 'expr' in gear and len(gear['expr'])>2:
        fields.append({'name':'Skill'   ,'value':gear['expr'] ,'inline':True})
    fields+=[
        {'name':'Stats'   ,'value':', '.join(['%s %s'%(val,key) for key,val in gear['stats'].items()]) ,'inline':False},
        #{'name':''   ,'value':'' ,'inline':True}
    ]

    embed.ConvertFields(fields)
    return embed